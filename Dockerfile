FROM bichselb/latex-plus

MAINTAINER Filip Gralinski <filipg@amu.edu.pl>

USER root

RUN apt-get -y update && apt-get -y install bc cpanminus hunspell hunspell-en-gb hunspell-en-us hunspell-pl libjpeg8-dev libxml-simple-perl libxml-writer-perl openjdk-8-jre-headless zlib1g-dev \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /tmp/xurl \
    && cd /tmp/xurl \
    && curl -L http://mirrors.ctan.org/macros/latex/contrib/xurl.zip -o xurl.zip \
    && unzip xurl.zip \
    && cd xurl \
    && mkdir /usr/share/texmf/tex/latex/xurl \
    && cp latex/xurl.sty /usr/share/texmf/tex/latex/xurl \
    && cd /tmp \
    && rm -rf /tmp/xurl \
    && texhash

WORKDIR /opt

RUN curl https://www.languagetool.org/download/LanguageTool-4.8.zip -o LanguageTool-4.8.zip \
    && unzip LanguageTool-4.8.zip \
    && rm LanguageTool-4.8.zip

COPY extras/languagetool /usr/bin/

RUN cpanm String::Util TAP::Harness::JUnit

RUN pip3 install --no-cache-dir proselint==0.10.2

RUN pip3 install --no-cache-dir syntok==1.2.2

ENV AUTOZOILPATH "/opt/autozoil"

RUN mkdir -p $AUTOZOILPATH/Autozoil

COPY Autozoil $AUTOZOILPATH/Autozoil
COPY autozoil.pl autozoil $AUTOZOILPATH/
COPY *.dic $AUTOZOILPATH/

WORKDIR $AUTOZOILPATH

CMD ./autozoil
