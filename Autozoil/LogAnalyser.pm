
package Autozoil::LogAnalyser;

# checking various typographic conventions

use utf8;
use strict;

my $MAXIMUM_UNDERFULL = 4999;

sub new {
    my ($class, $sink, $log_file) = @_;

    my $self = {
        'sink' => $sink,
        'log_file' => $log_file,
    };

    return bless $self, $class;
}

sub process {
    my ($self, $filename) = @_;

    my $log_filename = $self->get_log_filename($filename);

    my $sink = $self->{'sink'};

    if (! -r $log_filename) {
        print STDERR "no log file '$log_filename' found\n";
        return;
    }

    print STDERR "analysing '$log_filename\n";

    open my $logh,'<',$log_filename;

    while (my $line=<$logh>) {
        if (my ($too_wide, $line_number) =
                ($line =~ m{ Overfull \s \\hbox \s \((\d*.\d+)pt \s too \s wide\)
                             \s in \s paragraph \s at \s lines \s (\d+)--\d+ }x)) {
            $sink->add_mistake({
                'type' => 'latex',
                'label' => 'OVERFULL',
                'comment' => qq{overfull hbox ($too_wide too wide)},
                'line_number' => $line_number ,
            });
        }

        if (my ($badness, $line_number) =
                ($line =~ m{ Underfull \s \\hbox \s \(badness \s (\d+)\)
                             \s in \s paragraph \s at \s lines \s (\d+)--\d+ }x)) {
            if ($badness > $MAXIMUM_UNDERFULL) {
                $sink->add_mistake({
                    'type' => 'latex',
                    'label' => 'UNDERFULL',
                    'comment' => qq{underfull hbox (badness $badness)},
                    'line_number' => $line_number ,
                });
            }
        }

        if (my ($message) =
                ($line =~ m{^LaTeX Warning: (.*)$})) {
            if ($message !~ /may have changed/) {
                $sink->add_mistake({
                    'type' => 'latex',
                    'label' => 'WARNING',
                    'comment' => qq{$1},
                    'line_number' => 1
            });
            }
        }
    }

}

sub get_log_filename {
    my ($self, $filename) = @_;

    if (defined($self->{'log_file'})) {
        return $self->{'log_file'};
    }

    if ($filename !~ s{ \. [^\.]* $}{.log}x) {
        $filename .= '.log';
    }

    return $filename;
}

1;
