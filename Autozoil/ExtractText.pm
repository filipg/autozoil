
package Autozoil::ExtractText;

use strict;

sub new {
    my ($class) = @_;

    my $self = { };

    # detex leaves arguments of initial commands under Ubuntu
    my $good_detex = is_detex_good();

    $self->{'extra_command'} = ' ';

    if (!$good_detex) {
        $self->{'extra_command'} = q{|  perl -pne '$_.="\\n" if /\\\\end\\{(itemize|tabular)\\}|\\\\(sub)*section.*\\\\label|\\\\(centering|midrule)/ and !/^\\s*\\%/; $_="\\n" if /\\\\includegraphics/ and !/^\\s*\\%/; $_.="\\n\\n" if /\\\\end\\{(figure|table\\*?)\\}/ and !/^\\s*\\%/;'};
    }

    return bless $self, $class;
}

sub is_detex_good {
    my $result = qx{(echo '\\section{test}\\label{sec:test}'; echo ""; echo "Test") | detex -l};

    return $result =~ /\n.*\n.*\n/ms;
}

sub get_plain_text {
    my ($self, $filename) = @_;

    my $tmp_file = `mktemp`;
    chomp $tmp_file;
    $tmp_file .= '.txt';

    my $two_backslashes_quoted = q{\\\\\\\\};
    my $two_spaces = q{  };

    my $extra_command = $self->{'extra_command'};

    # detex zamienia \\ na znak końca wiersza, co psuje zgodność
    # numeracji wierszy, musimy to naprawić
    `perl -pne 's{$two_backslashes_quoted}{$two_spaces}g; s{ -- }{ -  }g; s{\\\\(?:noqa|noqall)\\{[^\\}]+\\}}{ }g;' < "$filename" $extra_command | perl -ne 'BEGIN{\$collected="";\$begindocfound=0;\$empties=""} if(\$begindocfound) { print } else { \$collected .= \$_; \$empties .= "\\n";}; if (/\\\\begin\\{document\\}/) { \$begindocfound = 1; print \$empties; } END{if(!\$begindocfound) { print \$collected; }} ' | detex -e "" -l -n - > "$tmp_file"`;
    # this is horrible... clean it up, please

    return $tmp_file;
}

1;
