
package Autozoil::Proselint;

use strict;

use Autozoil::ExtractText;
use JSON;

# not a thought police
my %unwanted_errors = map { $_ => 1} split/\n/,<< 'END_OF_UNWANTED_ERRORS';
glaad.terms
sexism.misc
END_OF_UNWANTED_ERRORS

sub new {
    my ($class, $sink, $language) = @_;

    my $self = {
        'sink' => $sink,
        'language' => $language,
        'extra_command' => '',
        'text_extractor' => Autozoil::ExtractText->new()
    };

    return bless $self, $class;
}

sub process {
    my ($self, $filename) = @_;

    my $language = $self->{'language'};

    # Only English
    if ($language ne 'en') {
        return 1;
    }

    my $out_tmp_file = `mktemp`;
    chomp $out_tmp_file;

    # detex zamienia \\ na znak końca wiersza, co psuje zgodność
    # numeracji wierszy, musimy to naprawić
    my $tmp_file = $self->{'text_extractor'}->get_plain_text($filename);

    # languagetool output has to processed because of some bug in
    # languagetool
    `proselint "$tmp_file" --json > "$out_tmp_file"`;

    my $json;
    {
        local $/; #Enable 'slurp' mode
        open my $fh, "<", $out_tmp_file;
        $json = <$fh>;
        close $fh;
    }
    my $data = decode_json($json);

    if (ref $data->{'data'}->{'errors'}) {
        for my $error (@{$data->{'data'}->{'errors'}}) {
            $self->process_error($error);
        }
    }
}


sub process_error {
    my ($self, $error) = @_;

    return if $unwanted_errors{$error->{'check'}};

    my $sink = $self->{'sink'};

    $sink->add_mistake({
        'line_number' => $error->{'line'},
        'beg' => $error->{'start'},
        'end' => $error->{'end'},
        'comment' => $error->{'message'},
        'correction' => $error->{'replacements'} ,
        'type' => 'proselint',
        'label' => $error->{'check'},
    });
}
