#!/usr/bin/perl

use strict;

use utf8;

use Test::More tests => 2;
use Test::Deep;

use Autozoil::Proselint;
use Autozoil::Sink::Store;

my $store_sink = Autozoil::Sink::Store->new();
my $checker = Autozoil::Proselint->new($store_sink, 'en');

$checker->process('Autozoil/Proselint/simple.tex');

ok(!$store_sink->is_ok());

cmp_set(
    [ $store_sink->get_all_mistakes() ],
    [
     {
         'type' => 'proselint',
         'line_number' => 7,
         'beg' => ignore(),
         'end' => ignore(),
         'comment' => ignore(),
         'label' => 'uncomparables.misc',
         'correction' => ignore(),
     },
     {
         'type' => 'proselint',
         'line_number' => 7,
         'beg' => ignore(),
         'end' => ignore(),
         'comment' => ignore(),
         'label' => 'weasel_words.very',
         'correction' => ignore()
     }
    ]);
