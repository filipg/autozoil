#!/usr/bin/perl

use strict;

use utf8;

use Test::More tests => 1;
use Test::Deep;
use Data::Dumper;

use Autozoil::Typo;
use Autozoil::Sink::Store;

my $store_sink = Autozoil::Sink::Store->new();
my $checker = Autozoil::Typo->new($store_sink, 'en');

$checker->process('Autozoil/Typo/english.tex');

ok($store_sink->is_ok());
